#!/usr/bin/env python
# John Blischak
# 2013-08-14

'''
Sorts fastq files after demultiplexing with Casava.
'''

import sys, os, shutil
try:
    import ipdb as pdb
except ImportError:   
    import pdb

################################################################################


def get_args(cline = []):
    import argparse
    parser = argparse.ArgumentParser(description = '''Sorts fastq files after
    demultiplexing with Casava.''')
    parser.add_argument('casava_dir', help = 'the directory with Casava output')
    parser.add_argument('new_dir', help = 'the new directory for transferring')
    parser.add_argument('-s', '--samplesheet', help = '''a custom csv file (only necessary if not
                                      using the SamplesDirectories.csv file in
                                      casava_dir)''', default = None)
    parser.add_argument('-c', '--copy', action = 'store_true', default = False)
    if len(cline) > 0:
        args = parser.parse_args(cline)
    else:
        args = parser.parse_args()
    return(args)

def parse_csv(line):
    cols = line.strip().split(',')
    flow_cell = cols[0]
    lane = cols[1]
    sample = cols[2]
    index_seq = cols[4]
    directory = cols[9]
    return [flow_cell, lane, sample, index_seq, directory]
    
def main(args):
    if (args.samplesheet == None):
        csv_name = os.path.join(args.casava_dir, 'SamplesDirectories.csv')
        if not os.path.exists(csv_name):
            args.casava_dir = os.path.join(args.casava_dir, 'Demultiplexed')
            csv_name = os.path.join(args.casava_dir, 'SamplesDirectories.csv')
    else:
        csv_name = args.samplesheet
    assert os.path.exists(csv_name), 'Unable to find csv file: %s'%(csv_name)
    csv_handle = open(csv_name)
    header = csv_handle.readline().strip().split(',')
    assert header == ['FCID', 'Lane', 'SampleID', 'SampleRef', 'Index', 'Description',
                      'Control', 'Recipe', 'Operator', 'Directory'], \
           'Columns have changed. Check the csv file!'
    for line in csv_handle:
        flow_cell, lane, sample, index_seq, directory = parse_csv(line)
        index_num = dic_index[index_seq]
        gerald = os.listdir(os.path.join(args.casava_dir, directory))
        assert len(gerald) == 1, 'Issue with GERALD directory location.'
        gerald = gerald[0]
        fname_orig = os.path.join(args.casava_dir,
                                  directory,
                                  gerald,
                                  's_%s_sequence.txt.gz'%(lane))
        assert os.path.exists(fname_orig), \
          'Unable to find original file: %s'%(fname_orig)
        sample_dir = os.path.join(args.new_dir, sample)
        if not os.path.exists(sample_dir):
            os.mkdir(sample_dir)
        fname_new = os.path.join(sample_dir,
                                 'fc-%s-lane-%s-index-%02d.fastq.gz'%(flow_cell,
                                                                      lane,
                                                                      index_num))
        if args.copy:
            shutil.copyfile(fname_orig, fname_new)
        else:
            if os.path.islink(fname_new):
                os.unlink(fname_new)
            os.symlink(fname_orig, fname_new)
##        pdb.set_trace()                                
    csv_handle.close()

## Illumina indices
## key: sequence, value: number
dic_index = {'ATCACG':1,
             'CGATGT':2,
             'TTAGGC':3,
             'TGACCA':4,
             'ACAGTG':5,
             'GCCAAT':6,
             'CAGATC':7,
             'ACTTGA':8,
             'GATCAG':9,
             'TAGCTT':10,
             'GGCTAC':11,
             'CTTGTA':12,
             'AGTCAA':13,
             'AGTTCC':14,
             'ATGTCA':15,
             'CCGTCC':16,
             'GTAGAG':17,
             'GTCCGC':18,
             'GTGAAA':19,
             'GTGGCC':20,
             'GTTTCG':21,
             'CGTACG':22,
             'GAGTGG':23,
             'GGTAGC':24,
             'ACTGAT':25,
             'ATGAGC':26,
             'ATTCCT':27,
             'CAAAAG':28,
             'CAACTA':29,
             'CACCGG':30,
             'CACGAT':31,
             'CACTCA':32,
             'CAGGCG':33,
             'CATGGC':34,
             'CATTTT':35,
             'CCAACA':36,
             'CGGAAT':37,
             'CTAGCT':38,
             'CTATAC':39,
             'CTCAGA':40,
             'GACGAC':41,
             'TAATCG':42,
             'TACAGC':43,
             'TATAAT':44,
             'TCATTC':45,
             'TCCCGA':46,
             'TCGAAG':47,
             'TCGGCA':48
             }
    
if __name__ == '__main__':
    args = get_args()
    ## For debugging:
    ## args = get_args(['/rawdata/Illumina_Runs/130807_SN_0795_0267_BC28G0ACXX',
    ##                  '/mnt/lustre/home/jdblischak/test/'])
    main(args)
