#!/usr/bin/env Rscript

# John Blischak
# 2013-05-20
#
# Creates a fasta file that contains all
# the exon-exon junctions in the given genome.
#
# For help run: ./make_junction_lib.R -h
#
# To make executable: chmod u+x make_junction_lib.R
#

## Input: Gene with two transcript models
## A----B   C-----D          H-------I J-K  L-----M   N---------O
## A----B   C-------E   F-G  H-------I J-K            N---------O

## Output: exon-exon junctions
## --BC--
## --BF-
## --BH--
## --BJ-
## --BL--
## --BN--
## --DF-
## --DH--
## --DJ-
## --DL--
## --DN--
## --EF-
## --EH--
## --EJ-
## --EL--
## --EN--
## -GH--
## -GJ-
## -GL--
## -GN--
## --IJ-
## --IL--
## --IN--
## -KL--
## -KN--
## --MN--

if (interactive()) {rm(list = ls())}
.libPaths(c(.libPaths(), '/home/jdblischak/R_libs'))
suppressPackageStartupMessages(library(biomaRt))
suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(BSgenome))
# For debugging:
## options(warn = 2) # Print warning messages as they occur
## options(error=quote({dump.frames('testdump3', TRUE); q()}))

################################################################################
# Command line arguments
################################################################################
parser <- ArgumentParser(description  = 'Creates a fasta file that contains all the exon-exon junctions in the given genome.')
parser$add_argument('-b', '--biomart', nargs = 1,
                    default = 'hsapiens_gene_ensembl',
                    help = 'Biomart for retrieving genome feature information [%(default)s]')
parser$add_argument('-s', '--seq', nargs = 1,
                    default = 'BSgenome.Hsapiens.UCSC.hg19',
                    help = 'BSgenome for retrieving genome sequences [%(default)s]')
parser$add_argument('-e', '--ensg', nargs = 1,
                    help = 'File containing list of Ensembl gene IDs in first column')
parser$add_argument('window', type = 'integer',
                    help = 'Window around each exon-exon junction [recommended: read length - 4]')
parser$add_argument('-o', '--outfile', nargs = 1, default = '',
                    help = 'File to write junctions')
################################################################################
# Functions
################################################################################

main <- function(args) {
  # Obtain gene information from Ensembl Biomart, call functions for junction
  # creation, and filter out inappropriate genes.
  
  # If writing to a file, make sure to overwrite since output.fasta always
  # appends to the output file.
  if (args$outfile != '') {file.create(args$outfile)}
  ensembl = useMart('ensembl', dataset = args$biomart)
  # Obtain list of genes
  if (is.null(args$ensg)) {
    genes <- getBM(attributes = c('ensembl_gene_id', 'source'),
                   mart = ensembl)
    genes <- genes[genes$source %in% c('ensembl', 'insdc'), 'ensembl_gene_id']
  } else {
    genes.data <- read.table(args$ensg, stringsAsFactors = FALSE)
    genes <- genes.data[grep('ENSG', genes.data[, 1]), 1]
  }
  
  for (g in genes) {
    write(g, file = stderr())
    exons <- getBM(attributes = c('chromosome_name',
                     'exon_chrom_start',
                     'exon_chrom_end',
                     'ensembl_transcript_id',
                     'rank',
                     'strand'),
                   filters = c('ensembl_gene_id'),
                   values = g, mart = ensembl)
    # Skip gene if it is not on canonical chromosomes
    if (!(sum(as.character(exons$chromosome_name[1]) %in% 
              c(1:23, 'X', 'Y', 'MT')))) {next}
    junctions <- make.junctions(exons, args$window)
    # Skip gene if it has no junctions
    if (class(junctions) != 'data.frame') {write(g, file = stderr()); next}
    seqs <- cbind(junctions, get.seqs(junctions, args$seq))
    output.fasta(seqs, out = args$outfile)
  }
}  

make.junctions <- function(exons, window) {
  # Return coordinates of all exon-exon junctions.
  #
  # exons: data.frame where each row is one exon of a gene transcript.
  #        The columns are chromosome_name, exon_chrom_start,
  #        exon_chrom_end, ensembl_transcript_id, rank, and strand.
  #
  # window: the number of base pairs to include of each exon.
  # 
  # result: data.frame where each row corresponds to an exon-exon junction.
  #         The columns are chr, up.start, up.end, down.start, and down.end.
  #
  
  # Remove dups
  exons.uniq <- subset(exons, 
                       !duplicated(paste0(exon_chrom_start, exon_chrom_end)))
  # Sort on 3' end
  exons.uniq.sort <- exons.uniq[order(exons.uniq$exon_chrom_end,
                                      exons.uniq$exon_chrom_start), ]
  len <- exons.uniq.sort$exon_chrom_end - exons.uniq.sort$exon_chrom_start + 1
  junctions <- vector()
  # Create exon-exon junctions for all long exons
  if (nrow(exons.uniq.sort) > 1) {
    for (i in 1:(nrow(exons.uniq.sort) - 1)) {
       for (j in (i+1):nrow(exons.uniq.sort)) {
        # If the start of the next exon is downstream
        if (exons.uniq.sort$exon_chrom_end[i] <
            exons.uniq.sort$exon_chrom_start[j]) {
          # Create an exon-exon junction with window bp of each exon
          chr <- exons.uniq.sort$chromosome_name[i]
          if (len[i] > window) {
            up.start <- exons.uniq.sort$exon_chrom_end[i] - window + 1
          } else {
            up.start <- exons.uniq.sort$exon_chrom_start[i]
          }
          up.end <- exons.uniq.sort$exon_chrom_end[i]
          down.start <- exons.uniq.sort$exon_chrom_start[j]
          if (len[j] > window) {
            down.end <- exons.uniq.sort$exon_chrom_start[j] + window - 1
          } else {
            down.end <- exons.uniq.sort$exon_chrom_end[j]
          }
          new.junc <- c(chr, up.start, up.end, down.start, down.end)
          junctions <- rbind(junctions, new.junc, deparse.level = 0)
        }
      }
    }
  }
  junctions <- data.frame(junctions, stringsAsFactors = FALSE)
  junctions[, -1] <- apply(junctions[, -1], 2, as.numeric)
  if (nrow(junctions) == 0) {return(NA)}
  colnames(junctions) <- c('chr', 'up.start', 'up.end',
                           'down.start', 'down.end')
  junctions.uniq <- subset(junctions, !duplicated(paste0(up.start, up.end, down.start, down.end)))
  #browser()
  return(junctions.uniq)
}  

get.seqs <- function(juncs, genome) {
  # Return sequences of each exon in a junction.
  #
  # juncs: data.frame where each row corresponds to an exon-exon junction.
  #        The columns are chr, up.start, up.end, down.start, and down.end.
  #        Can be obtained from make.junctions function.
  #
  # genome: The BSgenome library for extracting DNA sequence.
  #
  # result: data.frame where each row corresponds to an exon-exon junction.
  #         The columns are upstream and downstream, which contain the DNA
  #         sequence.
  #
  
  suppressPackageStartupMessages(require(genome, character.only = TRUE))
  upstream <- getSeq(eval(parse(text = genome)), 
                 names = paste0('chr', juncs$chr),
                 start = juncs$up.start,
                 end = juncs$up.end,
                 as.character = TRUE,
                 strand = rep('+', nrow(juncs)))
  downstream <- getSeq(eval(parse(text = genome)), 
                 names = paste0('chr', juncs$chr),
                 start = juncs$down.start,
                 end = juncs$down.end,
                 as.character = TRUE,
                 strand = rep('+', nrow(juncs)))
  seqs <- data.frame(upstream, downstream, stringsAsFactors = FALSE)
  return(seqs)
}  

output.fasta <- function(df, out = '') {
  # Write junction sequence in fasta format.
  #
  # df: data.frame or matrix where each row corresponds to one
  #     exon-exon junction.  The columns are chr, up.start, up.end,
  #     down.start, and down.end, upsstream, and downstream.
  #
  # out: where to write the results. Either to stdout (default) or
  #      the specified filename.
  #
  # result: each junction is appended to the file in fasta format, e.g.
  #         >chr20_61909860_61909905_61917006_61917015
  #         CCAGGTGTGAATTGGGGGTCTTGGGTGTAGAGAAGCGCTAACACTTGTGAAGGAGG
  #

  for (i in 1:nrow(df)) {
    cat('>chr', file = out, sep = '', append = TRUE)
    cat(paste(df$chr[i], df$up.start[i], df$up.end[i], df$down.start[i],
                     df$down.end[i], sep = '_'), file = out, append = TRUE)
    cat('\n', sep = '', file = out, append = TRUE)
    cat(paste0(df$upstream[i], df$downstream[i]), file = out, append = TRUE)
    cat('\n', sep = '', file = out, append = TRUE)    
  }
}  

################################################################################
# Script
################################################################################

if (interactive()) {
  ## args <- parser$parse_args(cline <- c('-e', 'test-genes4.txt',
  ##                                      '-o', 'test-results4.txt',
  ##                                      '46'))
  ## main(args)
} else {
  args <- parser$parse_args()
  main(args)
}
