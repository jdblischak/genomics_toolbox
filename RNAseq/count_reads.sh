#!/bin/bash
set -eu
# John Blischak
# 2013-07-26
# Counts number of reads per gene.
#
# Steps:
# 1) Count the number of reads overlapping each exon using intersectBed -c
# 2) Combine reads across exons using groupBy to get gene-level counts
#
# Use the -h command for usage instructions.
################################################################################

# Default options
file_sub=""
# Paths to programs	
BEDTOOLS_PATH="/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin"	

usage() {
  echo "Usage: `basename $0` [options] <exons bed file> <reads bed file>"
  echo "Options:"
  echo "  -h                   print this help"
  echo "  -s                   Output to a file that is the reads bed file modified
                               with the supplied sed substition instead of stdout"
}

while getopts ":hs:" opt
do
case $opt in
  h)
    usage
    exit 0
    ;;
  s)
    file_sub=$OPTARG  
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    exit 1
    ;;
  :)
    echo "Option -$OPTARG requires an argument." >&2
    exit 1
    ;;
esac
done  

# Shift the command line index to start at 1 for the first positional argument
shift $((OPTIND - 1))

if [ "$#" -ne 2 ]
then
  usage
  exit 2
else
  exons=$1
  echo "Exons file: $exons" >&2
  reads=$2
  echo "Reads file: $reads" >&2
fi

# Create new filename if supplied (-s option), otherwise write to
# stdout.
if [ -z $file_sub ]
then
  out=""
  echo "Writing results to stdout." >&2
else
  out=`echo $reads | sed $file_sub`
  echo "New filename: $out" >&2
fi

# Generate unique ID for naming temporary files that are stored in
# /tmp. This prevents any issues from arising if more than one job is
# run on the same node.
id=`date +%T`.$RANDOM # The time followed by a randomly generated number

#########
# Step 1: Count the number of reads overlapping each exon using intersectBed -c
#########

echo "Counting the number of reads overlapping each exon..." >&2
$BEDTOOLS_PATH/intersectBed -a $exons -b $reads -c > /tmp/$id.counts.bed

#########
# Step 2: Combine reads across exons using groupBy to get gene-level counts
#########

# First have to sort counts file by Ensembl gene ID in 4th column b/c
# groupBy only aggregates consecutive entries in the bed file (this
# problem would be caused by the exon of one gene lying in the intron
# of another).

sort -k 4 /tmp/$id.counts.bed > /tmp/$id.sorted.counts.bed

echo "Creating gene-level counts..." >&2

gene_counts() {
  groupBy -i $1 -g 4 -c 5 -o sum
}

if [ -z $out ]
then
  gene_counts /tmp/$id.sorted.counts.bed
else
  gene_counts /tmp/$id.sorted.counts.bed > $out
fi

echo "Finished successfully." >&2
exit 0






