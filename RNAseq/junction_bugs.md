# Documenting junction bugs

The script to create exon-exon junctions, `make_junction_lib.R`, has some strange warnings which are not reproducible, i.e. they are different each time, and also do not effect the results (two files with different Warnings are identical as assessed with `diff` command). Despite this, it would be nice to figure out why R is having these issues. The typical Warning message sent to stderr (`options(warn = 1))` is:

    ENSG00000257798
    ENSG00000179938
    Error in remove(list = objname, envir = cache) : 
      (converted from warning) object 'chr15' not found
    Calls: <Anonymous> -> remove
    ENSG00000040633
    ENSG00000156194


Sometimes the gene being processed is on the chromosome that in the warning message (e.g. the example above), however this is not always the case. Sometimes the gene previously processed is on the same chromosome, but it is harder to prove that it caused the error since I can only debug the current state of the program.

To hopefully figure out what the issue is, I am going to document the call stacks from loading a dump object and evaluating it with debugger (post-mortem debugging). I acheive this by adding the following lines to the script:

    # Convert warnings to errors
    options(warn = 2) 
    # When an error occurs, dump the current state of the environment
    # into the file testdump.rda, but continue running
    options(error=quote(dump.frames("testdump", TRUE); q()))



## Ex 1

Previously processed genes before the Warning:

+ ENSG00000177409
+ ENSG00000169251
+ ENSG00000257798
+ ENSG00000179938

The currently processed gene (ENSG00000179938) is on chr15.

	Message:  Error in remove(list = objname, envir = cache) : 
	  (converted from warning) object 'chr15' not found
	Calls: <Anonymous> -> remove
	Available environments had calls:
	1: main(args)
	2: output.fasta(seqs, out = args$outfile)
	3: cat(paste(df$chr[i], df$up.start[i], df$up.end[i], df$down.start[i], df$dow
	4: paste(df$chr[i], df$up.start[i], df$up.end[i], df$down.start[i], df$down.en
	5: standardGeneric("paste")
	6: unique(unlist(lapply(dots, methods:::.class1)))
	7: loadMethod(function (x, incomparables = FALSE, ...) 
	UseMethod("unique"), "u
	8: loadMethod(function (x, incomparables = FALSE, ...) 
	UseMethod("unique"), "u
	9: assign(".target", method@target, envir = envir)
	10: function (e) 
	{
		link_count <- get(objname, envir = link_counts, inherits 
	11: remove(list = objname, envir = cache)
	12: .signalSimpleWarning("object 'chr15' not found", quote(remove(list = objnam
	13: withRestarts({
		.Internal(.signalCondition(simpleWarning(msg, call), msg,
	14: withOneRestart(expr, restarts[[1]])
	15: doWithOneRestart(return(expr), restart)


## Ex 2

Previously processed genes before the Warning:

+ ENSG00000249481
+ ENSG00000232021
+ ENSG00000119523
+ ENSG00000204352

The currently processed gene (ENSG00000204352) is on chr9.

	Message:  Error in remove(list = objname, envir = cache) : 
	  (converted from warning) object 'chr9' not found
	Calls: <Anonymous> -> remove
	Available environments had calls:
	1: main(args)
	2: cbind(junctions, get.seqs(junctions, args$seq))
	3: standardGeneric("cbind")
	4: eval(quote(list(...)), env)
	5: eval(quote(list(...)), env)
	6: eval(expr, envir, enclos)
	7: get.seqs(junctions, args$seq)
	8: getSeq(eval(parse(text = genome)), names = paste0("chr", juncs$chr), start 
	9: getSeq(eval(parse(text = genome)), names = paste0("chr", juncs$chr), start 
	10: .local(x, ...)
	11: .extractFromBSgenomeSingleSequences(x, sseq_args$names, sseq_args$start, ss
	12: lapply(seq_len(length(grl)), function(i) {
		gr <- grl[[i]]
		if (length(
	13: lapply(seq_len(length(grl)), function(i) {
		gr <- grl[[i]]
		if (length(
	14: FUN(1[[1]], ...)
	15: x[[subject_name]]
	16: x[[subject_name]]
	17: .getBSgenomeSequence(name, x)
	18: .loadBSgenomeSequence(name, bsgenome)
	19: .loadSingleObject(name, seqs_dirpath, seqs_pkgname)
	20: load(filepath, envir = tmp_env)
	21: function (e) 
	{
		link_count <- get(objname, envir = link_counts, inherits 
	22: remove(list = objname, envir = cache)
	23: .signalSimpleWarning("object 'chr9' not found", quote(remove(list = objname
	24: withRestarts({
		.Internal(.signalCondition(simpleWarning(msg, call), msg,
	25: withOneRestart(expr, restarts[[1]])
	26: doWithOneRestart(return(expr), restart)

## Ex 3

This error occurred after the last genes were processed.

+ ENSG00000262260
+ ENSG00000262796
+ ENSG00000262599
+ ENSG00000262024

But even though this was different (especially that is was an error
and not a warning), the results were still unchanged as assessed by
`diff`.

	Message:  Error: object 'indow' not found
	Available environments had calls:
	1: 
	0: 

## Ex 4

The previous three genes processed:

+ ENSG00000204406
+ ENSG00000188681
+ ENSG00000136522

The previously processed gene (ENSG00000188681) is on chr21.

	Message:  Error in remove(list = objname, envir = cache) : 
	  (converted from warning) object 'chr21' not found
	Calls: <Anonymous> -> remove
	Available environments had calls:
	1: main(args)
	2: cbind(junctions, get.seqs(junctions, args$seq))
	3: standardGeneric("cbind")
	4: eval(quote(list(...)), env)
	5: eval(quote(list(...)), env)
	6: eval(expr, envir, enclos)
	7: get.seqs(junctions, args$seq)
	8: getSeq(eval(parse(text = genome)), names = paste0("chr", juncs$chr), start 
	9: getSeq(eval(parse(text = genome)), names = paste0("chr", juncs$chr), start 
	10: .local(x, ...)
	11: .extractFromBSgenomeSingleSequences(x, sseq_args$names, sseq_args$start, ss
	12: lapply(seq_len(length(grl)), function(i) {
		gr <- grl[[i]]
		if (length(
	13: lapply(seq_len(length(grl)), function(i) {
		gr <- grl[[i]]
		if (length(
	14: FUN(1[[1]], ...)
	15: x[[subject_name]]
	16: x[[subject_name]]
	17: .getBSgenomeSequence(name, x)
	18: .loadBSgenomeSequence(name, bsgenome)
	19: .loadSingleObject(name, seqs_dirpath, seqs_pkgname)
	20: load(filepath, envir = tmp_env)
	21: function (e) 
	{
		link_count <- get(objname, envir = link_counts, inherits 
	22: remove(list = objname, envir = cache)
	23: .signalSimpleWarning("object 'chr21' not found", quote(remove(list = objnam
	24: withRestarts({
		.Internal(.signalCondition(simpleWarning(msg, call), msg,
	25: withOneRestart(expr, restarts[[1]])
	26: doWithOneRestart(return(expr), restart)


