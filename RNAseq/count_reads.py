#!/usr/bin/env python
# John Blischak
# 2013-07-25

'''
Counts the number of reads per gene.
'''

import sys, pdb
import pybedtools as bt

################################################################################

def get_args():
    import argparse
    parser = argparse.ArgumentParser(description = '''Counts the number of reads
             overlapping the union of exons for each gene.''')
    parser.add_argument('exons', help = 'the bed file of exons')
    parser.add_argument('reads', help = 'the bed file of reads')
    args = parser.parse_args()
    return(args)

def collapse_reads_to_bases(infile, outfile):
    for line in infile:
        cols = line.strip().split('\t')
        cols[2] = str(int(cols[1]) + 1)
        outfile.write('\t'.join(cols) + '\n')

def main():
    args = get_args()
    # Collapse the reads just to their first base
    in_reads = open(args.reads, 'r')
    out_bases = open('/tmp/tmp.bed', 'w')
    collapse_reads_to_bases(in_reads, out_bases)
    in_reads.close()
    out_bases.close()
    reads = bt.BedTool('/tmp/tmp.bed')
    assert [x['end'] - x['start'] for x in reads[:10]] == [1]*10, \
           'Reads were not properly collapse to their leading base.'
    # Count the number of overlapping reads per exon
    exons = bt.BedTool(args.exons)
    exon_counts = exons.intersect(reads, c = True)
    # Combine exon read counts and lengths to gene level counts and length
    gene_counts = exon_counts.groupby(g = [1, 4], c = [2, 3, 5], ops = ['sum']*3)
    # print gene_counts >> 
    pdb.set_trace()

if __name__ == '__main__':
    main()
