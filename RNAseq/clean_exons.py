#!/usr/bin/env python
# John Blischak
# 2013-05-03

'''
Merges the features of a bed file and returns only those merged features that
have the same name attribute (e.g. exons of the same gene).
'''

import sys, pdb
import pybedtools as bt

################################################################################

def get_args():
    import argparse
    parser = argparse.ArgumentParser(description = '''Collapses transcript models
             to one set of unique exons per gene. Also removes all regions that
             overlap more than one gene model''')
    parser.add_argument('bed_file', help = 'the bed file of exons')
    args = parser.parse_args()
    return(args)

def all_same(items):
    '''
    Returns True if all the items are the same value. Otherwise returns False.
    http://stackoverflow.com/questions/3787908/python-determine-if-all-items-of-a-list-are-the-same-item
    '''
    return all(x == items[0] for x in items)

def output_bed(bed):
    '''
    Sends a BedTool to stdout.
    Only allows one name in the name field (there are potentially
    multiple after merging exons).
    '''
    for x in bed:
        x.name = x.name.split(';')[0]
        sys.stdout.write('\t'.join(x) + '\n')

def main():
    args = get_args()
    bed = bt.BedTool(args.bed_file)
    bed_sorted = bed.sort()
    bed_merged = bed_sorted.merge(nms = True)
    bed_clean = bed_merged.filter(lambda x: all_same(x.name.split(';')))
    #pdb.set_trace()
    output_bed(bed_clean)

if(__name__=='__main__'):
    main()
