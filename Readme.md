# Genomics Toolbox

###Description:
Resuable scripts that perform common genomics analyses.

###Currently:
* Check read quality with fastQC
* Map reads with BWA
* Create bed file that contains the merged exonic sequences that are unique to each gene (for RNAseq)

###To make a file executable:
    :::bash
       chmod u+x filename

###To learn how to use a script:
    :::bash
       ./filename -h

###Contact:
John Blischak
jdblischak at gmail dot com